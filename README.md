# React Simple Crud (tweets version)

* CRUD with Axios (GET, DELETE, POST. No Put) 
* Custom Hooks 
* Component-based approach
* Controlled and Uncontrolled forms

## Installation

```
git clone https://gitlab.com/react-typescript/react-simple-crud-tweets.git
npm install
npm start
```

## Run server

After installation, open a new terminal and run the server

```
npm run server
```

## Try the app

Visit [http://localhost:3000/](http://localhost:3000/) in your web browser and use the app
