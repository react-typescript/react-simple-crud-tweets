import React  from 'react';

export const ErrorMsg: React.FC<{hasError: boolean}> = (props) => {
  return props.hasError ? <div className="alert alert-danger">Server error</div> : null
};
