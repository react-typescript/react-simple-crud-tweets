import React  from 'react';
import { Friend } from '../model/friend';

export const Total: React.FC<{ friends: Friend[]}> = props => {

  const getTotal = () => {
    return props.friends.reduce((acc: number, item: Friend) => {
      console.log(acc, item.tweets)
      return acc + item.tweets;
    }, 0)
  };

  return (
    <div className=" mr-3 text-center">
      <strong className="badge badge-info">
        {getTotal()} TWEETS
      </strong>
    </div>
  )
};
