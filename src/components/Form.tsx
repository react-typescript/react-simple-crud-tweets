import React, { useState } from 'react';
import cn from 'classnames';
import { Friend } from '../model/friend';

const INITIAL_STATE = { name: '', tweets: 0};

interface FormsProps {
  onAdd: (name: Friend) => void;
}
export const Form: React.FC<FormsProps> = props => {
  const [data, setData] = useState<Friend>(INITIAL_STATE)
  const [dirty, setDirty] = useState<boolean>(false)
  const valid = data.name.length > 0;

  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    console.log('data', data)
    e.preventDefault();
    if (valid) {
      props.onAdd(data);
      setData(INITIAL_STATE)
    }
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDirty(true);
    setData({
      ...data,
      [e.currentTarget.name]: e.currentTarget.type === 'number' ?
          +e.currentTarget.value : e.currentTarget.value
    })
  }

  return (
    <form onSubmit={submitHandler} >
      <input
        className={cn(
          'form-control',
          { 'is-invalid': !valid && dirty}
        )}
        type="text"
        name="name"
        value={data.name}
        onChange={onChange}
        placeholder="Write something and press enter"
      />

      <input
        className={cn(
          'form-control',
          { 'is-invalid': !valid && dirty}
        )}
        type="number"
        name="tweets"
        value={data.tweets}
        onChange={onChange}
      />

      <button type="submit">ADD</button>
    </form>
  )
};

