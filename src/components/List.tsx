import React  from 'react';
import { Friend } from '../model/friend';

interface ListProps {
  friends: Friend[];
  onDelete: (friend: Friend) => void;
}

export const List: React.FC<ListProps> = props => {
  return (
    <div>
      {
        props.friends.map((item: Friend, index: number) => {
          console.log(item)
          return (
            <li key={item.id} className="list-group-item">
              {index + 1}. {item.name}
              <div className="pull-right">
                <div className="badge badge-dark mr-2">
                  {item.tweets}
                </div>
                <i className="fa fa-trash" onClick={() => props.onDelete(item)} />
              </div>
            </li>
          )
        })
      }
    </div>
  )
};
