import React, { useEffect, useRef } from 'react';
import { Friend } from '../model/friend';

interface FormsProps {
  onAdd: (name: Friend) => void;
}
export const FormUncontrolled: React.FC<FormsProps> = props => {
  const inputEl = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputEl.current?.focus()
  });

  const onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if(e.key === 'Enter' && e.currentTarget.value.length > 0) {
      props.onAdd({ name: e.currentTarget.value, tweets: 0 })
      e.currentTarget.value = '';
    }
  };

  return (
    <div >
      <input
        className="form-control"
        type="text"
        ref={inputEl}
        onKeyPress={onKeyPress}
        placeholder="Write something and press enter"
      />

      <hr/>
    </div>
  )
};
