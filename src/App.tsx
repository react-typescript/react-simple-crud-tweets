import React from 'react'
import { ErrorMsg } from './components/ErrorMsg';
import { Total } from './components/Total';
import { List } from './components/List';
import { useUsers } from './hooks/useUsers';
import { Form } from './components/Form';
// import { FormUncontrolled } from './components/FormUncontrolled';

const App: React.FC = () => {
  const { error, friends, addFriend, deleteFriend } = useUsers();

  return (
    <div>
      <Form onAdd={addFriend}/>
      {/*<FormUncontrolled onAdd={addFriend}/>*/}
      <List friends={friends} onDelete={deleteFriend} />
      <ErrorMsg hasError={error} />
      <Total friends={friends} />
    </div>
  )
};
export default App;
