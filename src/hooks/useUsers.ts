import React, { useEffect } from 'react';
import Axios from 'axios';
import { Friend } from '../model/friend';
const API_URL = ' http://localhost:3001/tweets';

export function useUsers() {
  const [friends, setFriends] = React.useState<Friend[]>([]);
  const [error, setError] = React.useState<boolean>(false);

  useEffect(() => {
    Axios.get<Friend[]>(API_URL)
      .then(res => {
        setFriends(res.data);
        setError(false);
      })
      .catch(err => setError(true))
  }, []);

  const deleteFriend = (user: Friend) => {
    Axios.delete(`${API_URL}/${user.id}`)
      .then(() => {
        setFriends(friends.filter(u => u.id !== user.id));
        setError(false);
      })
      .catch(err => setError(true))
  };

  const addFriend = (friend: Friend) => {
    Axios.post<Friend>(`${API_URL}`, friend)
      .then((res) => {
        setFriends([...friends, res.data]);
        setError(false);
      })
      .catch(err => setError(true))
  }
  return {
    friends,
    error,
    addFriend,
    deleteFriend
  }
}
